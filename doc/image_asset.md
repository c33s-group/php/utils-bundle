# Image Asset Twig Extension

`home.html.twig`:
```twig
<img src=" {{ asset_image('https://placekitten.com/g/200/300?image=2')|imagine_filter('your_filter_name') }}" alt="">
```

## Quickstart

add `- '%kernel.cache_dir%/%twig.image_asset.cache_dir_suffix%'` to your
`liip_imagine` loaders section.

`liip_imagine.yaml`:
```yaml
liip_imagine:
   loaders:
        default:
            filesystem:
                data_root:
                    - ...
                    - '%kernel.cache_dir%/%twig.image_asset.cache_dir_suffix%'
```

You cannot use the constant expression together with `%kernel.cache_dir%`. the
additional parameter `twig.image_asset.cache_dir_suffix` from this bundle is
required.

to change the suffix override `twig.image_asset.cache_dir_suffix` in your project.


## Extra Config

the bundles `services.yaml` binds the following paramerter.
`services.yaml`
```yaml
services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        bind:
            string $cacheDir: "%kernel.cache_dir%"
            string $locale: "@=service('request_stack').getCurrentRequest().getLocale()"
            string $imageAssetCacheDirSuffix: "%twig.image_asset.cache_dir_suffix%"
```
