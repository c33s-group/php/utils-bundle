<?php

declare(strict_types=1);

namespace Tests\Unit;

use C33s\Bundle\UtilsBundle\Twig\ImageAssetExtension;
use Codeception\Test\Unit;
use Symfony\Bridge\Twig\Extension\AssetExtension;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Mime\MimeTypes;
//use Symfony\Component\String\Slugger\AsciiSlugger;
use Cocur\Slugify\Slugify;

/**
 * @group twig
 */
class ImageAssetExtensionTest extends Unit
{
    private $cacheDir;

    public function _before(): void
    {
        $this->cacheDir = codecept_output_dir('tmp/'.uniqid('', false));
    }

    /**
     * @dataProvider provideData
     */
    public function testImageWebNamerFolder(string $url, string $expected, string $cacheDirSuffix = ''): void
    {
        if ('' !== $cacheDirSuffix) {
            $cacheDirSuffix = '/'.$cacheDirSuffix;
        }
        $actual = $this->getImageAssetHelperExtension($this->cacheDir.$cacheDirSuffix)->assetImage($url);
        self::assertEquals($actual, $expected);
    }

    public function provideData(): array
    {
        return [
            ['https://placekitten.com/g/200/300?image=2', 'placekitten.com/g/200/300-image-2.jpg', ''],
            ['https://picsum.photos/id/1/200/300', 'picsum.photos/id/1/200/300.jpg', ''],
            ['https://loremflickr.com/320/240/kitten', 'loremflickr.com/320/240/kitten.jpg', ''],
            ['https://loremflickr.com/320/240/kitten?lock=1', 'loremflickr.com/320/240/kitten-lock-1.jpg', ''],
        ];
    }

    private function getImageAssetHelperExtension($cacheDir = null): ImageAssetExtension
    {
        $locale = 'de';

        if (null === $cacheDir) {
            $cacheDir = codecept_output_dir('tmp');
        }
        $assetExtension = new AssetExtension(new Packages(new Package(new EmptyVersionStrategy())));
//        $slugger = new AsciiSlugger();
        $slugger = new Slugify();
        $mimeTypes = new MimeTypes();
        $filesystem = new Filesystem();

        return new ImageAssetExtension(
            $assetExtension,
            $cacheDir,
            $slugger,
            $filesystem,
            $mimeTypes,
            $locale
        );
    }
}
