<?php

namespace Helper;

use Tests\Traits\CodeceptionSymfonyTrait;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Cookie;

class Functional extends \Codeception\Module
{
    use CodeceptionSymfonyTrait;

    /**
     * Sets the Host which will be simulated to access. This is necessary to access routes which only listen to a specific
     * host.
     * If not set `localhost` will be used.
     *
     * @param $host
     */
    public function setHost($host)
    {
        $this->getClient()->setServerParameter('HTTP_HOST', $host);
    }

    /**
     * Auto Login a User without creating it in the Database with writing directly to session.
     *
     * doesn't work with Groups
     *
     * @param string $user
     * @param string $firewallContext
     * @param array  $roles
     */
    public function loggedInAs($user = 'admin', $firewallContext = 'main', $roles = ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER', 'ROLE_SONATA_ADMIN'])
    {
        /** @var Session $session */
        $session = $this->getContainer()->get('session');
        $client = $this->getClient();

        $token = new UsernamePasswordToken($user, null, $firewallContext, $roles);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);
    }

    /**
     * Checks if the last url access was a redirect to the $url given in this method. To check if also the correct status
     * code was sent, provide it by $expectedStatusCode.
     *
     * redirects have to be off for this working '$I->followRedirects(false)' before the first url is visited.
     * ```
     * <?php
     * $I->followRedirects(false);
     * $I->amOnPage('/admin/dashboard');
     * $I->seeRedirectTo('/admin/login');
     * ?>
     * ```
     *
     * https://github.com/gamajo/codeception-redirects/blob/develop/src/Redirects.php
     *
     * @param string   $url
     * @param int|null $expectedStatusCode
     *
     * @throws \Exception
     */
    public function seeRedirectTo($url, $expectedStatusCode = null)
    {
        $client = $this->getClient();

        if ($client->isFollowingRedirects()) {
            throw new \Exception('Follow redirects must be on false "$I->followRedirects(false);" before visiting the page to test');
        }
        $targetLocation = $client->getResponse()->headers->get('location');
        if ($expectedStatusCode) {
            $this->assertEquals($expectedStatusCode, $client->getResponse()->getStatusCode());
        }
        $this->assertSame(0, stripos(strrev($targetLocation), strrev($url)));
    }

    /**
     * @see Functional::seeRedirectTo
     *
     * @param $url
     */
    public function seeTemporaryRedirectTo($url)
    {
        $this->seeRedirectTo($url, 302);
    }

    /**
     * @see Functional::seeRedirectTo
     *
     * @param $url
     */
    public function seePermanentRedirectTo($url)
    {
        $this->seeRedirectTo($url, 301);
    }

    /**
     * Ensures that no redirect has taken place after accessing a url.
     *
     * @throws \Exception
     */
    public function haveNoRedirect()
    {
        $client = $this->getClient();

        if ($client->isFollowingRedirects()) {
            throw new \Exception('Follow redirects must be on false "$I->followRedirects(false);" before visiting the page to test');
        }

        $this->assertEquals(false, $client->getResponse()->headers->get('location', false));
        $this->assertNotEquals(301, $client->getResponse()->getStatusCode());
        $this->assertNotEquals(302, $client->getResponse()->getStatusCode());
    }

    /**
     * Toggle redirections on and off.
     *
     * By default, BrowserKit will follow redirections, so to check for 30*
     * HTTP status codes and Location headers, they have to be turned off.
     *
     * @since 1.0.0
     *
     * @param bool $followRedirects Optional. Whether to follow redirects or not.
     *                              Default is true.
     */
    public function followRedirects($followRedirects = true)
    {
        $client = $this->getClient();
        $client->followRedirects($followRedirects);
    }
}
