<?php

namespace Group;

use Codeception\Event\TestEvent;
use Tests\Traits\CodeceptionSymfonyTrait;

/**
 * Group class is Codeception Extension which is allowed to handle to all internal events.
 * This class itself can be used to listen events for test execution of one particular group.
 * It may be especially useful to create fixtures data, prepare server, etc.
 *
 * INSTALLATION:
 *
 * To use this group extension, include it to "extensions" option of global Codeception config.
 */
class Wip extends \Codeception\Platform\Group
{
    use CodeceptionSymfonyTrait;

    public static $group = 'wip';

    public function _before(TestEvent $e)
    {
        $this->writeln('');
        $this->writeln('+ Wip Group');
        $this->loadFixtures();
    }

    public function _after(TestEvent $e)
    {
    }

    protected function loadFixtures()
    {
        //do something here
    }
}
