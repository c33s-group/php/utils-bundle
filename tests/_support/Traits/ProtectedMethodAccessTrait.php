<?php

namespace Tests\Traits;

trait ProtectedMethodAccessTrait
{
    /**
     * Returns the previous protected method from a class as public method, which now can be invoked.
     *
     * ```php
     * $service = new ExampleService();
     * $method = self::getProtectedMethod('exampleMethodName', ExampleService::class);
     * $params = []
     * $result = $method->invokeArgs($service, $params)
     * ```
     *
     * @param $methodName
     * @param $className
     *
     * @return \ReflectionMethod
     */
    protected static function getProtectedMethod($methodName, $className)
    {
        $class = new \ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * Runs a protected method from a class and returns its result.
     *
     * ```php
     * $params = []
     * $result = self::callProtectedMethod('exampleMethodName', ExampleService::class, $params);
     * ```
     *
     * @param $methodName
     * @param $className
     * @param $methodParameters
     *
     * @return mixed
     */
    protected static function callProtectedMethod($methodName, $className, $methodParameters = [])
    {
        $method = self::getProtectedMethod($methodName, $className);
        $object = new $className($methodParameters);

        return $method->invokeArgs($object, $methodParameters);
    }
}
