<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Twig;

use C33s\Bundle\UtilsBundle\Helper\RequestStackHelper;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RequestStackTwigExtension extends AbstractExtension
{
    /** @var LoggerInterface */
    protected $logger;

    /** @var RequestStackHelper */
    protected $requestStackHelper;

    /**
     * RequestStackTwigExtension constructor.
     */
    public function __construct(LoggerInterface $logger, RequestStackHelper $requestStackHelper)
    {
        $this->logger = $logger;
        $this->requestStackHelper = $requestStackHelper;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('is_master_request', [$this, 'isMasterRequest']),
            new TwigFunction('is_sub_request', [$this, 'isSubRequest']),
            new TwigFunction('is_embedded', [$this, 'isSubRequest']),
            new TwigFunction('request_stack', [$this, 'getRequestStack']),
            new TwigFunction('master_request', [$this, 'getMasterRequest']),
            new TwigFunction('current_request', [$this, 'getCurrentRequest']),
            new TwigFunction('parent_request', [$this, 'getParentRequest']),
            new TwigFunction('master_request_route_name', [$this, 'getMasterRouteName']),
            new TwigFunction('master_request_query_parameter', [$this, 'getMasterQueryParameter']),
        ];
    }

    public function isMasterRequest(): bool
    {
        return $this->requestStackHelper->isMasterRequest();
    }

    public function isSubRequest(): bool
    {
        return $this->requestStackHelper->isSubRequest();
    }

    public function getRequestStack(): RequestStack
    {
        return $this->requestStackHelper->requestStack;
    }

    public function getMasterRequest(): Request
    {
        return $this->requestStackHelper->requestStack->getMasterRequest();
    }

    public function getCurrentRequest(): Request
    {
        return $this->requestStackHelper->requestStack->getCurrentRequest();
    }

    public function getParentRequest(): Request
    {
        return $this->requestStackHelper->requestStack->getParentRequest();
    }

    public function getMasterRouteName(): string
    {
        return $this->requestStackHelper->requestStack->getParentRequest()->get('_route');
    }

    public function getMasterQueryParameter(): array
    {
        return $this->requestStackHelper->requestStack->getParentRequest()->query->all();
    }

    public function getName()
    {
        return 'request_stack_helper';
    }
}
