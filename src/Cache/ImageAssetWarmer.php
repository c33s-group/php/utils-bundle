<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Cache;

use RuntimeException;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;

final class ImageAssetWarmer implements CacheWarmerInterface
{
    /**
     * @var string
     */
    private $imageAssetCacheDirSuffix;

    public function __construct(string $imageAssetCacheDirSuffix)
    {
        $this->imageAssetCacheDirSuffix = $imageAssetCacheDirSuffix;
    }

    /**
     * @param string $cacheDir
     */
    public function warmUp($cacheDir): array
    {
        $dir = $cacheDir.'/'.$this->imageAssetCacheDirSuffix;
        if (!is_dir($dir) && !mkdir($dir) && !is_dir($dir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }

        return [];
    }

    public function isOptional(): bool
    {
        return false;
    }
}
