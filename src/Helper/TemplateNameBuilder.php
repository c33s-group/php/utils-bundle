<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Helper;

use InvalidArgumentException;
use Webmozart\PathUtil\Path;

class TemplateNameBuilder
{
    /** @var RequestStackHelper */
    protected $requestStackHelper;

    /**
     * TemplateNameBuilder constructor.
     */
    public function __construct(RequestStackHelper $requestStackHelper)
    {
        $this->requestStackHelper = $requestStackHelper;
    }

    public function build(string $templateName, string $suffix = '_embedded', string $extension = '.html.twig')
    {
        if ($this->requestStackHelper->isMasterRequest()) {
            return $templateName;
        }

        $filename = Path::getFilename($templateName);
        if (!str_ends_with($filename, $extension)) {
            $defaultInfo = "the default extension is '.html.twig'";
            $message = "template name '${filename}' must contain the given extension '${extension}'. $defaultInfo";
            throw new InvalidArgumentException($message);
        }

        $directory = Path::getDirectory($templateName);
        $filename = Path::getFilenameWithoutExtension($templateName, $extension);
        $templateName = $directory.'/'.$filename.$suffix.$extension;

        return $templateName;
    }
}
