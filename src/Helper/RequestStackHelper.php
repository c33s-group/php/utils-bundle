<?php

declare(strict_types=1);

namespace C33s\Bundle\UtilsBundle\Helper;

use Symfony\Component\HttpFoundation\RequestStack;

class RequestStackHelper
{
    /** @var RequestStack */
    public $requestStack;

    /**
     * TemplateNameBuilder constructor.
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function isMasterRequest(): bool
    {
        if (null === $this->requestStack->getParentRequest()) {
            return true;
        }

        return false;
    }

    public function isSubRequest(): bool
    {
        return !$this->isMasterRequest();
    }
}
